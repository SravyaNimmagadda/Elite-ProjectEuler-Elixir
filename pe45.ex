defmodule Euler45 do
  def tri?(n) do
    Enum.map(285..100000, fn(n) -> round(n * (n + 1) / 2) end)
    |> Enum.member?(n)
  end
  def pent?(n) do
    Enum.map(165..100000, fn(n) -> round(n * (3 * n - 1) / 2) end)
    |> Enum.member?(n)
  end
  def hex?(n) do
    Enum.map(143..100000, fn(n) -> round(n * (2 * n - 1)) end)
    |> Enum.member?(n)
  end
  def solve do
  IO.puts Stream.iterate(40756, &(&1 + 1))
  |> Enum.find(fn(n) -> pent?(n) and hex?(n) end)
  #1533776804
  end
end
