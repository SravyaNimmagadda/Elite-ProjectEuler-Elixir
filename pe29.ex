defmodule DistinctPowers do
  def powers(a, b) do
    Enum.map(a, fn c -> Enum.map(b, fn d -> :math.pow(c, d) end) end)
  end
end

IO.puts(Enum.count(Enum.uniq(List.flatten(DistinctPowers.powers(2..100, 2..100)))))
