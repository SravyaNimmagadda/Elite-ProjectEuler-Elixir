defmodule NumberLetterCounts do
  def to_words(n) do
    ones = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
    tens = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
    #hundreds = ["hundred"]
    cond do
      n < 20 ->
         Enum.fetch!(ones, n)
      n > 19 and n < 100 ->
        a = Enum.fetch!(tens, round(Float.floor((n / 10) - 2)))
        b = Enum.fetch!(ones, round(rem(n, 10)))
        a <> b
      n > 99 and n < 1000 ->
        a = Enum.fetch!(ones, round(Float.floor(n / 100)))
        b = a <> "hundredand"
        c = NumberLetterCounts.to_words(rem(n, 100))
        b <> c
      n == 1000 ->
        "onethousand"
      true ->
        "range isn't specified"
    end
  end
  IO.puts (Enum.map(1..1000, fn(x) -> NumberLetterCounts.to_words(x) end)
         |> Enum.map(fn(x) -> String.length(x) end)
         |> Enum.sum) - 27 #-27 cuz the "and" after hundred repeats nine times

end
