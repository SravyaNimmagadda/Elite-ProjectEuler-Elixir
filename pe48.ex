defmodule SelfPowers do
  require Integer

  def pow(_, 0), do: 1
  def pow(x, n) when Integer.is_odd(n), do: x * pow(x, n - 1)
  def pow(x, n) do
    result = pow(x, div(n, 2))
    result * result
  end

  IO.inspect 1..1000
  |> Enum.filter(fn(n) -> rem(n, 10) != 0 end)
  |> Enum.map(fn(n) -> SelfPowers.pow(n, n) end)
  |> Enum.sum
  |> Integer.digits
  |> Enum.take(-10)
  |> Integer.undigits
end
