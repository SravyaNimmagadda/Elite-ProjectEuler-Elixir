defmodule Permutations do
 def solve do
     perms([1, 2, 3, 4, 5, 6, 7, 8, 9])

   end

   def perms([]), do: [[]]
   def perms(list) do
     for head <- list, tail <- perms(list -- [head]), do: [head|tail]
   end
 end
