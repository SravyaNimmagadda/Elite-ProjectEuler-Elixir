defmodule DigitFifthPowers do
  def fifthPower(x) do
    :math.pow(x, 5)
  end
  def solve do
    Stream.iterate(10, &(&1 + 1))
    |> Enum.take_while(fn(x) -> x < 120825 end)
    |> Integer.digits
    |> Enum.reduce(fn(x) -> fifthPower(x) end)
    |> Enum.sum
  end
end

IO.inspect DigitFifthPowers.solve
