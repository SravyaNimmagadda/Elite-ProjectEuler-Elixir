defmodule CodedTriangularNumbers do
#@fname "p042_words.txt"

  def tri?(n) do
    Enum.map(1..10000, fn(n) -> round(n * (n + 1) / 2) end)
    |> Enum.member?(n)

  end


  def letterIndex(x) do

    alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    alphabet
    |> Enum.find_index(fn(n) -> n == x end)

  end

  def wordValue(x) do
    x
    |> String.graphemes
    |> Enum.map(fn(n) -> CodedTriangularNumbers.letterIndex(n) + 1 end)
    |> Enum.sum
  end

  def solve do
    IO.gets("Enter Word:")
    |> String.trim
    |> String.downcase
    |> CodedTriangularNumbers.wordValue
    |> CodedTriangularNumbers.tri?
 end

end
