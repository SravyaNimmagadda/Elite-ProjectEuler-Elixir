defmodule LexographicPermutation do
  def solve do
    perms([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    |> Enum.take(1000000)
    |> Enum.sort
    |> List.last
    |> Enum.join
  end

  def perms do
    def of([]) do
      [[]]
    end

    def of(list) do
      for h<- list, t <- of(list -- [h]), do: [h | t]
    end
  end
end
