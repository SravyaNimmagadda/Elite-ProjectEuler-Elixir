defmodule Primes do
  def prime?(x) when x in [2, 3, 5, 7, 11, 13, 17], do: true
  def prime?(x) when x < 2, do: false
  def prime?(x) do
    Enum.all?((2..round(:math.sqrt(x))), fn(n) -> rem(x, n) != 0 end)
  end
end
