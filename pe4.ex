 def solve do
    Enum.reduce(1..999, 0, fn(n, max) ->
      Enum.reduce(n..999, max, fn(m, max) ->
        keep_largest_palindrome(n*m, max)
      end)
    end)
  end
  
  def keep_largest_palindrome(product, max) do
    if product > max and palindrome?(product) do
      product
    else
      max
    end
  end
  
  def palindrome?(number) do
    string = Integer.to_string(number)
    string == String.reverse(string)
  end


solution = fn -> solve end
IO.puts solution
