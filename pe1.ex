multiple? = fn val, n -> rem(val, n) == 0 end

sum = 1..999 |> Enum.filter(&(multiple?.(&1, 3) or multiple?.(&1, 5))) |> Enum.sum

IO.puts(sum)
