def prime(1, _), do: []
def prime(n, i) do
  if rem(n, i) == 0, do: [i] ++ prime(div(n, i), i), else: prime(n, i + 1)
end

largest = prime(600851475143, 2) |> Enum.max

IO.puts largest
  
