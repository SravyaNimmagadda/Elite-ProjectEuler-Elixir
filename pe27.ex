defmodule QuadraticPrimes do
  def quadratic(n, a, b) do
    (n * n) + (a * n) + b
  end

  def prime?(x) when x in [2, 3, 5, 7, 11, 13, 17], do: true
  def prime?(x) when x < 2, do: false
  def prime?(x) do
    Enum.all?((2..round(:math.sqrt(x))), fn(n) -> rem(x, n) != 0 end)
  end

  def noOfConsecutivePrimes(a,b) do
    Stream.iterate(0, &(&1 + 1))
    |> Enum.take_while(fn(n) -> prime?(quadratic(n, a, b)) end)
    |> Enum.count
  end

  def generateResult do
    resultSet = for a <- -999..999, b <- abs(a)..1000, do: [a, b, QuadraticPrimes.noOfConsecutivePrimes(a,b)]
    Enum.max_by(resultSet, fn([a, b, c]) -> List.last([a, b, c]) end)
  end
end



IO.inspect QuadraticPrimes.generateResult


